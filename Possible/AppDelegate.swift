//
//  AppDelegate.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import UIKit
import ASXMLParser

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, XMLParserDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        return true
    }
    
}

