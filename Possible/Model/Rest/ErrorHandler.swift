//
//  ErrorHandler.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation

enum PossibleErrors: Error {
    case NoInternetConnectionError
    case InvalidURLError
    case GeneralError
    case NoDataError
    case ParseFailedError
}

class ErrorHandler : NSObject {
    static func messageForError(error: Error) -> String {
        switch error {
        case PossibleErrors.NoInternetConnectionError:
            return "No internet connection"
        case PossibleErrors.InvalidURLError:
            return "The URL you've entered is invalid"
        case PossibleErrors.GeneralError:
            return "Something went wrong, please try again later"
        case PossibleErrors.NoDataError:
            return "No data"
        case PossibleErrors.ParseFailedError:
            return "Failed to handle response"
        default:
            return messageForError(error: PossibleErrors.GeneralError)
        }
    }
}
