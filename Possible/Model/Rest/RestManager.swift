//
//  RestManager.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation
import Alamofire
import FXReachability
import JSONJoy

class RestManager : NSObject {

    // MARK : Singleton
    static let sharedInstance = RestManager()
    
    fileprivate override init() {
    }
    
    func fetchFeed(feedURL: String, completion : ( (_ data: Data?, _ error: Error?) -> ())? ) {
        guard FXReachability.isReachable() else {
            completion?(nil, PossibleErrors.NoInternetConnectionError)
            return
        }
        
        guard let url = URL(string: feedURL) else {
            completion?(nil, PossibleErrors.InvalidURLError)
            return
        }
        
        Alamofire.request(url).responseData { (response: DataResponse<Data>) in
            if response.result.isFailure {
                completion?(nil, PossibleErrors.GeneralError)
            } else {
               completion?(response.data!, nil)
                
            }
        }
    }
}
