//
//  UserDefaultsStore.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation

class UserDefaultsStore {
    static let kFeedKey = "feed"
    
    static func saveFeed(feed: RSSFeed) {
        
        var feedsToSave = [feed]
        
        if let previousFeedArchive = UserDefaults.standard.data(forKey: UserDefaultsStore.kFeedKey),
            var previousFeed =  NSKeyedUnarchiver.unarchiveObject(with: previousFeedArchive) as? [RSSFeed] {
            previousFeed.append(feed)
            feedsToSave = previousFeed
        }
        
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: feedsToSave)
        UserDefaults.standard.set(encodedData, forKey: UserDefaultsStore.kFeedKey)
    }
    
    
    static func remove(feed: RSSFeed) {
        var feedsToSave: [RSSFeed] = []
        
        if let previousFeedArchive = UserDefaults.standard.data(forKey: UserDefaultsStore.kFeedKey),
            let previousFeed =  NSKeyedUnarchiver.unarchiveObject(with: previousFeedArchive) as? [RSSFeed] {
            feedsToSave = previousFeed.filter(){$0.feedId != feed.feedId}
        }
        
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: feedsToSave)
        UserDefaults.standard.set(encodedData, forKey: UserDefaultsStore.kFeedKey)
    }
    
    
    static func getFeeds() -> [RSSFeed] {
        if let previousFeedArchive = UserDefaults.standard.data(forKey: UserDefaultsStore.kFeedKey),
            let feeds =  NSKeyedUnarchiver.unarchiveObject(with: previousFeedArchive) as? [RSSFeed] {
            
            return feeds
        } else {
            return []
        }
    }
}
