//
//  XMLParser.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation
import ASXMLParser


class RSSXMLParser: IParserInterace {
    
    let kRSSKEy = "rss"
    let kChannelKey = "channel"
    let kTitleKey = "title"
    let kImageKey = "image"
    let kTextKey = "text"
    let kDescriptionKey = "description"
    let kURLKey = "url"
    let kPubDateKey = "pubDate"
    let kLinkKey = "link"
    let kItemsKey = "item"
    
    var delegate: ParserDelegate?
    
    func parseData(data: Any?) {

        guard let data = data as? Data else {
            delegate?.parserDidFail()
            return
        }
        
        do {
            let parsedXML = try XMLReader.dictionary(forXMLData: data, options: UInt(XMLReaderOptionsProcessNamespaces))
            if let rss = parsedXML[kRSSKEy] as? NSDictionary,
                let channel = rss[kChannelKey] as? NSDictionary {
                
                let (title, descr, imageURL) = parseChannelInfo(channel: channel)
                let feedItem = RSSFeed(title: title, decr: descr, imageURL: imageURL)
                
                if let items = channel[kItemsKey] as? NSArray {
                    feedItem.items = parseFeed(items: items)
                }
                
                delegate?.parserDidFinish(rssFeed: feedItem)
            } else {
                delegate?.parserDidFail()
            }
        }
        catch {
            delegate?.parserDidFail()
        }
        
    }
    
    func parseChannelInfo(channel: NSDictionary) -> (title: String?, descr: String?, imageURL: String?) {
        var title: String?
        var descr: String?
        var imageURL: String?
        
        if let titleDict = channel[kTitleKey] as? NSDictionary {
            title = titleDict[kTextKey] as? String
            
        }
        
        if let descrDict = channel[kDescriptionKey] as? NSDictionary {
            descr = descrDict[kTextKey] as? String
            
        }
        
        if let imageDict = channel[kImageKey] as? NSDictionary,
            let urlDict = imageDict[kURLKey] as? NSDictionary {
            
            imageURL = urlDict[kTextKey] as? String
            
        }
        
        return (title, descr, imageURL)
    }
    
    func parseFeed(items: NSArray) -> [RSSFeedItem] {
        
        var retArray: [RSSFeedItem] = []
        
        if let items = items as? [NSDictionary] {
            
            var pubDate: String?
            var title: String?
            var contentURL: String?
            
            for item in items {
                if let pubDateDict = item[kPubDateKey] as? NSDictionary {
                    pubDate = pubDateDict[kTextKey] as? String
                }
                
                if let titleDict = item[kTitleKey] as? NSDictionary {
                    title = titleDict[kTextKey] as? String
                }
                
                if let linkDict = item[kLinkKey] as? NSDictionary {
                    contentURL = linkDict[kTextKey] as? String
                }
                
                let feedItem = RSSFeedItem(pubDate: pubDate, title: title, contenURL: contentURL)
                retArray.append(feedItem)
            }
        }
        
        return retArray
        
    }
}
