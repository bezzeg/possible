//
//  IParserInterface.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation

protocol ParserDelegate {
    func parserDidFinish(rssFeed: RSSFeed)
    func parserDidFail()
}

protocol IParserInterace {
    var delegate: ParserDelegate? {get set}
    
    func parseData(data: Any?)
}
