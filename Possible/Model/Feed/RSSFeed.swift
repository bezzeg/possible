//
//  File.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation

class RSSFeed: NSObject, NSCoding {
    
    let kTitleKey = "title"
    let kDescriptionKey = "description"
    let kImageURLKey = "imageURL"
    let kIDKey = "id"
    let kItemsKey = "items"
    let kFeedURLKey = "feedURL"
    
    var feedTitle: String?
    var feedDescription: String?
    var feedImageURL: String?
    var feedId: String!
    var feedURL: String!
    
    var items: [RSSFeedItem] = []
    
    init(title: String?, decr: String?, imageURL: String?) {
        self.feedId = UUID().uuidString
        self.feedTitle = title
        self.feedDescription = decr
        self.feedImageURL = imageURL
    }
    
    required init(coder decoder: NSCoder) {
        self.feedTitle = decoder.decodeObject(forKey: kTitleKey) as? String
        self.feedDescription = decoder.decodeObject(forKey: kDescriptionKey) as? String
        self.feedImageURL = decoder.decodeObject(forKey: kImageURLKey) as? String
        self.feedId = decoder.decodeObject(forKey: kIDKey) as! String
        self.feedURL = decoder.decodeObject(forKey: kFeedURLKey) as? String ?? ""
        self.items = decoder.decodeObject(forKey: kItemsKey) as? [RSSFeedItem] ?? []
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.feedTitle, forKey: kTitleKey)
        coder.encode(self.feedDescription, forKey: kDescriptionKey)
        coder.encode(self.feedImageURL, forKey: kImageURLKey)
        coder.encode(self.feedId, forKey: kIDKey)
        coder.encode(self.items, forKey: kItemsKey)
        coder.encode(self.feedURL, forKey: kFeedURLKey)
    }
}
