//
//  MyFeedInteractor.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation

protocol MyFeedInteractorProtocol {
    func feedDidFetch(feed: RSSFeed)
    func feedDidFailToFetch(error: Error)
}

class MyFeedInteractor: NSObject {
    var delegate: MyFeedInteractorProtocol?
    var currentFeedURL: String!
    
    func fetchRSSInfoForURL(feedURL: String, parser: IParserInterace) {
        RestManager.sharedInstance.fetchFeed(feedURL: feedURL) { (data: Data?, error: Error?) in
            
            self.currentFeedURL = feedURL
            if error != nil {
                self.delegate?.feedDidFailToFetch(error: error!)
            } else if data == nil {
                self.delegate?.feedDidFailToFetch(error: PossibleErrors.NoDataError)
            } else {
                var parser = parser
                parser.delegate = self
                parser.parseData(data: data!)
            }
        }
    }
    
    func loadFeeds() -> [RSSFeed] {
        return UserDefaultsStore.getFeeds()
    }
    
    func storeFeed(feed: RSSFeed) {
        UserDefaultsStore.saveFeed(feed: feed)
    }
    
    func removeFeed(feed: RSSFeed) {
        UserDefaultsStore.remove(feed: feed)
    }
}

extension MyFeedInteractor: ParserDelegate {
    func parserDidFail() {
        delegate?.feedDidFailToFetch(error: PossibleErrors.ParseFailedError)
    }
    
    func parserDidFinish(rssFeed: RSSFeed) {
        rssFeed.feedURL = self.currentFeedURL
        delegate?.feedDidFetch(feed: rssFeed)
    }
}
