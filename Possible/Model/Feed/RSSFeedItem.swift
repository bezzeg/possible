//
//  RSSFeedItem.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation

class RSSFeedItem: NSObject, NSCoding {
    
    let kPublishDateKey = "publishDate"
    let kTitleKey = "title"
    let kContentKey = "content"
    
    var itemPublishedDate: String?
    var itemTitle: String?
    var itemContentURL: String?
    
    init(pubDate: String?, title: String?, contenURL: String?) {
        self.itemPublishedDate = pubDate
        self.itemTitle = title
        self.itemContentURL = contenURL
    }
    
    required init(coder decoder: NSCoder) {
        self.itemPublishedDate = decoder.decodeObject(forKey: kPublishDateKey) as? String
        self.itemTitle = decoder.decodeObject(forKey: kTitleKey) as? String
        self.itemContentURL = decoder.decodeObject(forKey: kContentKey) as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.itemPublishedDate, forKey: kPublishDateKey)
        coder.encode(self.itemTitle, forKey: kTitleKey)
        coder.encode(self.itemContentURL, forKey: kContentKey)
    }
}
