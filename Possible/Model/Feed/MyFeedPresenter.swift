//
//  MyFeedPresenter.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation

protocol MyFeedPresenterInterface {
    func fetchFeed(feedURL: String)
    func loadStoredFeeds()
    func removeFeed(feed: RSSFeed)
}

class MyFeedPresenter: NSObject {
    var viewDelegate: MyFeedViewInterface!
    var interactor = MyFeedInteractor()
    
    override init() {
        super.init()
        interactor.delegate = self
    }
}

extension MyFeedPresenter: MyFeedPresenterInterface {
    func fetchFeed(feedURL: String) {
        viewDelegate.showLoading()
        interactor.fetchRSSInfoForURL(feedURL: feedURL, parser: RSSXMLParser())
    }
    
    func loadStoredFeeds() {
        let feeds = interactor.loadFeeds()
        self.viewDelegate.showLoadedFeeds(feeds: feeds)
    }
    
    func removeFeed(feed: RSSFeed) {
        self.interactor.removeFeed(feed: feed)
        self.loadStoredFeeds()
    }
}

extension MyFeedPresenter: MyFeedInteractorProtocol {
    func feedDidFetch(feed: RSSFeed) {
        viewDelegate.feedDidArrive(feed: feed)
        interactor.storeFeed(feed: feed)
        viewDelegate.hideLoading(completion: nil)
    }
    
    func feedDidFailToFetch(error: Error) {
        viewDelegate.hideLoading { 
            self.viewDelegate.feedDidFailToGet(message: ErrorHandler.messageForError(error: error))
        }
    }
}
