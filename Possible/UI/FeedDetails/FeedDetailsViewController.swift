//
//  FeedDetailsViewController.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation
import UIKit

class FeedDetailsViewController: UIViewController {

    //MARK:- Constants
    let kTitle = "Details"
    
    //MARK:- Outlets
    @IBOutlet weak var webView: UIWebView!
    
    //MARK:- Variables
    var feedItem: RSSFeedItem!
    
    //MARK:- Lifecycle
    override func viewDidLoad() {
        self.title = feedItem.itemTitle ?? kTitle
        if let content = feedItem.itemContentURL,
            let url = URL(string: content) {
            webView.loadRequest(URLRequest(url: url))
        }
    }
}
