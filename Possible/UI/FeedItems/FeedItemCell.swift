//
//  FeedItemCell.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation
import UIKit

class FeedItemCell: UITableViewCell {
    
    //MARK:- Constants
    static let NIB_NAME = "FeedItemCell"
    static let REUSE_ID = "feedItemCellId"
    let kMissingTitleText = "N/A"
    
     //MARK:- Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    //MARK:- Public functions
    func setupWithFeedItem(feedItem: RSSFeedItem) {
        titleLabel.text = feedItem.itemTitle ?? kMissingTitleText
        dateLabel.text = feedItem.itemPublishedDate ?? kMissingTitleText
    }
}
