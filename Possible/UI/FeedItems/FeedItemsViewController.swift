//
//  FeedDetailsViewController.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation
import UIKit

class FeedItemsViewController: UIViewController {
    
    //MARK:- Constants
    let kFeedItemCellId = "feedItemCell"
    let kDetailsSegueId = "detailsSegue"
    let kDefaultEstimatedHeight: CGFloat = 80
    
    //MARK:- Outlets
    @IBOutlet weak var feedItemsTableView: UITableView!
    
    //MARK:- Variables
    var feed: RSSFeed!
    var selectedFeedItem: RSSFeedItem!
    
    //MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    //MARK:- Private functions
    private func setupTableView() {
        feedItemsTableView.delegate = self
        feedItemsTableView.dataSource = self
        feedItemsTableView.estimatedRowHeight = kDefaultEstimatedHeight
        feedItemsTableView.rowHeight = UITableViewAutomaticDimension
        feedItemsTableView.register(UINib(nibName: FeedItemCell.NIB_NAME, bundle: nil), forCellReuseIdentifier: FeedItemCell.REUSE_ID)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == kDetailsSegueId {
            let targetVC = segue.destination as! FeedDetailsViewController
            targetVC.feedItem = selectedFeedItem
        }
    }
    
    //MARK:- Actions
    @IBAction func shareButtonPressed(_ sender: UIBarButtonItem) {
        let text = feed.feedURL
        let textToShare = [text]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
}

//MARK:- UITableViewDataSource
extension FeedItemsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feed.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedItemCell.REUSE_ID) as! FeedItemCell
        
        let feedItem = feed.items[indexPath.row]
        cell.setupWithFeedItem(feedItem: feedItem)
        
        return cell
    }
}

//MARK:- UITableViewDelegate
extension FeedItemsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedFeedItem = feed.items[indexPath.row]
        performSegue(withIdentifier: kDetailsSegueId, sender: self)
    }
}
