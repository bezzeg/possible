//
//  ViewController.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import UIKit
import FTIndicator

protocol MyFeedViewInterface {
    func feedDidArrive(feed: RSSFeed)
    func feedDidFailToGet(message: String)
    func showLoadedFeeds(feeds: [RSSFeed])
    func showLoading()
    func hideLoading(completion: (() -> ())?)
}

class MyFeedsViewController: UIViewController {

    //MARK:- Constants
    let kFeedItemsSegueId = "feedItemsSegue"
    let kEnterFeedText = "Enter RSS feed URL"
    let kLoadingMessageTitle = "Loading"
    let kDefaultEstimatedHeight: CGFloat = 40
    
     //MARK:- Outlets
    @IBOutlet weak var feedTableView: UITableView!
    
    //MARK:- Variables
    var selectedFeed: RSSFeed!
    var feedList: [RSSFeed] = []
    var eventHandler: MyFeedPresenterInterface!
    
    //MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupPresenter()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == kFeedItemsSegueId {
            let targetVC = segue.destination as! FeedItemsViewController
            targetVC.feed = selectedFeed
        }
    }
    
    //MARK:- Private functions
    private func setupTableView() {
        feedTableView.delegate = self
        feedTableView.dataSource = self
        feedTableView.estimatedRowHeight = kDefaultEstimatedHeight
        feedTableView.rowHeight = UITableViewAutomaticDimension
        feedTableView.register(UINib(nibName: FeedCell.NIB_NAME, bundle: nil), forCellReuseIdentifier: FeedCell.REUSE_ID)
    }
    
    private func setupPresenter() {
        let presenter = MyFeedPresenter()
        eventHandler = presenter
        presenter.viewDelegate = self
        presenter.loadStoredFeeds()
    }

    //MARK:- Actions
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        self.createAlertWithTextField(title: kEnterFeedText) { (text: String) in
            self.eventHandler.fetchFeed(feedURL: text)
        }
    }
}

//MARK:- UITableViewDataSource
extension MyFeedsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedCell.REUSE_ID) as! FeedCell
        
        let feed = feedList[indexPath.row]
        cell.setupWithFeed(feed: feed)
        
        return cell
    }
}

//MARK:- UITableViewDelegate
extension MyFeedsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            let feed = feedList[indexPath.row]
            eventHandler.removeFeed(feed: feed)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedFeed = feedList[indexPath.row]
        performSegue(withIdentifier: kFeedItemsSegueId, sender: self)
    }
}


//MARK:- MyFeedViewInterface
extension MyFeedsViewController: MyFeedViewInterface {
    func feedDidArrive(feed: RSSFeed) {
        feedList.append(feed)
        feedTableView.reloadData()
    }
    
    func feedDidFailToGet(message: String) {
        self.showErrorMessage(message: message)
    }
    
    func showLoadedFeeds(feeds: [RSSFeed]) {
        self.feedList = feeds
        feedTableView.reloadData()
    }
    
    func showLoading() {
        FTProgressIndicator.showProgressWithmessage(kLoadingMessageTitle)
    }
    
    func hideLoading(completion: (() -> ())?) {
        FTProgressIndicator.dismiss()
        completion?()
    }
}

