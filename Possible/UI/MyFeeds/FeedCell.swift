//
//  FeedCell.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation
import AlamofireImage
import UIKit

class FeedCell: UITableViewCell {
    
    //MARK:- Constants
    static let NIB_NAME = "FeedCell"
    static let REUSE_ID = "feedCellId"
    let kMissingTitleText = "N/A"
    let kMissingDescriptionText = "No description available"
    
    //MARK:- Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    //MARK:- Public functions
    func setupWithFeed(feed: RSSFeed) {
        titleLabel.text = feed.feedTitle ?? kMissingTitleText
        descriptionLabel.text = feed.feedDescription ?? kMissingDescriptionText
        if let thumbnailURL = feed.feedImageURL,
            let downloadURL = URL(string: thumbnailURL) {
            feedImageView.af_setImage(withURL: downloadURL)
        } else {
            feedImageView.image = UIImage(named: "feed_icon")
        }
    }
}
