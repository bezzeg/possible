//
//  UIViewControllerExtension.swift
//  Possible
//
//  Created by Bezzeg Mihály on 19/03/17.
//  Copyright © 2017 Bezzeg Mihály. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showErrorMessage(message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func createAlertWithTextField(title: String, completion: ( (_ message: String) -> ())? ) {
        let alertController = UIAlertController(title: title, message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            if let textField = alertController.textFields?[0],
                let enterdText = textField.text {
                completion?(enterdText)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
